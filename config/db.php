<?php

return [
    'development' => [
        'driver'    => 'mysql',
        'host'      => 'gerenciador-financas-mysql',
        'database'  => 'gerenciador_financas',
        'username'  => 'gerenciador',
        'password'  => 'gerenciador',
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci'
    ]
];